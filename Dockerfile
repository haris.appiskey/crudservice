FROM openjdk:8-jdk-alpine
FROM maven:alpine
VOLUME /tmp
COPY target/main-0.0.1-SNAPSHOT.jar bootapp.jar
ENTRYPOINT ["java","-jar","bootapp.jar"]
EXPOSE 8080