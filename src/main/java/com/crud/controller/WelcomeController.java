package com.crud.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.Filter;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping(value = "${app.url}"+"/welcome")
public class WelcomeController {
    @RequestMapping(value={"/",""}, method = RequestMethod.GET)
    public String getAppTitle(HttpServletRequest request, HttpServletResponse resp) throws IOException {
//request.getHeader("host");
        System.out.println(request.getHeader("host"));
        String requestURL = String.valueOf(request.getRequestURL());
        return "<center><h1 style='margin-top:200px'> Welcome to Dev Environment "+requestURL+" </h1></center>";
    }

}
