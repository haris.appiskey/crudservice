package com.crud.controller;

import com.crud.dao.AccountDao;
import com.crud.model.Account;
import com.crud.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "${app.url}"+"/account")
public class AccountController{

    @Autowired
    AccountService accountService;

    @GetMapping("/hello")
    public String hello(){
        return "Hello World";
    }

    @GetMapping("/list")
    private List<Account> getAccounts()
    {
        return accountService.getAccounts();
    }

    @PostMapping("/save")
    public ResponseEntity saveAccount(@RequestBody Account account) {
        accountService.insert(account);
        return new ResponseEntity<String>("You Successfully saved the object", HttpStatus.CREATED);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity getAccount(@PathVariable("id") int accountid)
    {
        Account account = accountService.getAccountById( accountid);

        return ResponseEntity.status(HttpStatus.OK).body(account);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteAccount(@PathVariable("id") int accountid)
    {
        accountService.delete(accountid);
        return ResponseEntity.status(HttpStatus.OK).body("You Successfully delete the object");
    }

}