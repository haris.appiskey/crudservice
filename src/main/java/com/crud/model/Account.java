package com.crud.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="account")
@Getter @Setter
public class Account {
    @Id
    private int id;
    private String username;
}