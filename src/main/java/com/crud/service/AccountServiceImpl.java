package com.crud.service;

import com.crud.dao.AccountDao;
import com.crud.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

   private AccountDao accountDao;

   @Autowired
    public void accountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public List<Account> getAccounts() {
        List<Account> listAccount = new ArrayList<>();
        listAccount = (List<Account>) accountDao.findAll();
        return listAccount;
    }

    @Override
    public void insert(Account account) {
        accountDao.save(account);
    }

    public Account getAccountById(int id)
    {
        return accountDao.findById(id).get();
    }

    public void delete(int id)
    {
        accountDao.deleteById((int) id);
    }
}


