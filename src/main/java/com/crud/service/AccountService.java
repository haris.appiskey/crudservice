package com.crud.service;

import com.crud.model.Account;

import java.util.List;

public interface AccountService {

    List<Account> getAccounts();
    void insert(Account Account);
    Account getAccountById(int id);
    public void delete(int id);
    }

