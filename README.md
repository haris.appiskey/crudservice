**Monitoring Spring Boot Applications with Prometheus and Grafana**

This small demo project contains an example setup of Prometheus and Grafana to monitor Spring Boot applications.

The project contains a default Grafana Prometheus datasource and scrapes the Spring Boot project and Prometheus server for monitoring information.

If you want to login to Grafana you can use the admin / password combination.

For monitoring Spring Boot applications I highly recommend the JVM Micrometer dashboard created by Michael Weirauch.

**Building the project**

First build the spring boot application.

**Run**

$ docker-compose build && docker-compose up


**After all services have started successfully, you can navigate to the following urls:**

- Spring Boot app - http://localhost:8080/actuator
- Prometheus - http://localhost:9090/
- Grafana - http://localhost:3000/
